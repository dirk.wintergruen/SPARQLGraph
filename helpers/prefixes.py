import rdflib

NAMESPACE_SPARQL_PREFIX="""
        prefix owl: <http://www.w3.org/2002/07/owl#>
        prefix crm: <http://erlangen-crm.org/160714/>
        prefix sr: <http://ontologies.mpiwg-berlin.mpg.de/scholarlyRelations/>
        prefix skos: <http://www.w3.org/2004/02/skos/core#>
        PREFIX RDF: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX RDFS: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX ASTRENT: <http://entities.mpiwg-berlin.mpg.de/astro/datenbank/>
        PREFIX ASTRONT: <http://ontologies.mpiwg-berlin.mpg.de/astro/>
        
        """
OWL = rdflib.Namespace("http://www.w3.org/2002/07/owl#")
CRM = rdflib.Namespace("http://erlangen-crm.org/160714/")
SR = rdflib.Namespace("http://ontologies.mpiwg-berlin.mpg.de/scholarlyRelations/")
SKOS = rdflib.Namespace("http://www.w3.org/2004/02/skos/core#")