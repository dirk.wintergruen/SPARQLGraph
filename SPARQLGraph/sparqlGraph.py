'''
Created on 10.11.2016

@author: dwinter
'''

import igraph
import SPARQLWrapper
import logging
import rdflib
from _collections import defaultdict
from rdflib.plugins.stores.sparqlstore import SPARQLStore
try: #try first the old location
    from BlazeGraphHandler.BlazeGraphHandler import BlazeGraphStore,BlazeGraphHandlerException
except:
    try:
        from BlazeGraphHandler import BlazeGraphStore,BlazeGraphHandlerException
    except:
        from BlazeGraphHandler.BlazeGraphHandler.BlazeGraphHandler import BlazeGraphStore
from rdflib.term import URIRef
from multiprocessing import Pool
from network_extensions.igraphx import union

logger = logging.getLogger(__name__)

                
class WrongNodeQueryException(Exception):
    pass


def generateBipartiteFromSparqlProcess(data):
            self = data[0]
            #print(data[3])

            return SPARQLGraph.generateBipartiteFromSparql(self, "", data[1], data[2], binding=data[3],
                                                           gr=igraph.Graph(),
                                                           **data[4])

class SPARQLGraph(SPARQLWrapper.SPARQLWrapper2,rdflib.ConjunctiveGraph):
    
    prefixes= """
        PREFIX RDF: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX RDFS: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX ASTRENT: <http://entities.mpiwg-berlin.mpg.de/astro/datenbank/>
        PREFIX ASTRONT: <http://ontologies.mpiwg-berlin.mpg.de/astro/>
        prefix ont: <http://ontologies.mpiwg-berlin.mpg.de/scholarlyRelations/>
        prefix crm: <http://erlangen-crm.org/160714/>
    """
    def __init__(self, repositoryUrl=None, defaultGraph=None,blazegraph=True,store=None,user=None,passwd=None,**kwargs):
        
        if repositoryUrl:
            SPARQLWrapper.SPARQLWrapper2.__init__(self, repositoryUrl)#, defaultGraph=defaultGraph)

        else:
            SPARQLWrapper.SPARQLWrapper2.__init__(self, store.query_endpoint)  # , defaultGraph=defaultGraph)
        self.user = user
        self.passwd = passwd
        if blazegraph and defaultGraph is None:
            raise BlazeGraphHandlerException("if blazegraph is true, defaultGraph has to be set") 

        if store is None:
            if blazegraph:
                store = BlazeGraphStore(repositoryUrl,defaultGraph=defaultGraph)
            else:
                store = SPARQLStore(repositoryUrl)
            
        rdflib.ConjunctiveGraph.__init__(self, store=store, identifier = defaultGraph)
        #igraph.Graph.__init__(self,**kwargs)
        self.igraph=igraph.Graph(**kwargs)
        if defaultGraph is None:
            logging.warn("default is none, this will break queries at blazegraph!")
    timeout = False
    
    def __getattr__(self,name): # handle missing attributeds and assume they belong to igraph
        return getattr(self.igraph,name)
    
    def query(self,query_object=None,**kwargs):
        """ make sure to use the right query method"""
        
        if query_object is None:
            return SPARQLWrapper.SPARQLWrapper2.query(self)
        else:
            return rdflib.Graph.query(self, query_object, **kwargs)
    
    @property
    def  vs(self):
        return self.igraph.vs
    
    @property
    def es(self):
        return self.igraph.es
    
    def addNodesFromSparql(self, query, typ, counter=0):
        """
        This methods performs a search in the attached triple store and creates nodes.
        
        :param str query: sparql query which returns the nodes. The query must return only one variable in the bindings.
        :param str typ: a string which will be come a vertex attribute "type"
        :param int counter: (optional) a integer to where the number of added nodes will be added to.
        :return int: Number of added nodes (+startvalue if counter is set as parameter)
        
        """
        self.setQuery(query)
        res = self.query()
        for r in res.bindings:
            node_string = []
            if len(r) > 1:
                raise WrongNodeQueryException("Query should only return one uri per result!")
                
            for s in r:    
                self.igraph.add_vertex(r[s].value)
                n = self.vs.find(name=r[s].value)
                n["type"] = typ
               
                
                counter += 1
                # v["typ"]=typ
        return counter
    
    def addRelationsFromSparql(self, query, paramSource, paramTarget, typ=None, limit=None, paramType=None, generateVertices=False, vertex_type=None, counter=0,fh=None,relationParamList=[],avoid_double=True):
        """
        Creates edges (and vertices= from a sparql query)
        :param str query: SPARQL querxy
        :param str paramSource: variable in the query which describes the source of a edge
        :param str paramTarget variable in the query which describes the target of a edge
        :param str paramType:  (alternative to typ) variable in the query which describes the attribute typ of a edge
        :param str typ:  (alternative to paramType) string the becomes the attr type of the a edge
        :param str limit:  (optional) maximal number of query results
        :param bool generateVertices: (default = False)
        :param str vertex_type: (optional) if set sets attribute "type" of all newly created vertices to type
        :param int counter: (optional) set if counter shouldn't start with 0
        :param FileHandeler fh: (optional) if set method only creates a file with s,type,t per line.
        """
        logger.debug("logging")
        if limit is None:
            self.setQuery(self.prefixes+query)
            res = self.query()
            logger.debug("Len: %s"%len(res.bindings))
            return self.generateGraph(res.bindings,paramSource,
                                      paramTarget,
                                      paramType,
                                      typ=typ,
                                      counter=counter,
                                      fh=fh,
                                      vertex_type=vertex_type,
                                      generateVertices=generateVertices,
                                      relationParamList = relationParamList,
                                      avoid_double = avoid_double)
        else:
            bs=[]
            offset=0
            end = False
            res=[]
            while end==False:
                qr=query+" limit %s offset %s"%(limit,offset*limit)
                self.setQuery(qr)
                res = self.query()
                logger.debug("Len: %s"%len(res.bindings))
                if len(res.bindings)==0:
                    end=True
                else:
                    counter=self.generateGraph(res.bindings,
                                               paramSource,
                                               paramTarget,
                                               paramType,
                                               typ=typ,
                                               counter=counter,
                                               fh=fh,vertex_type=vertex_type,
                                               generateVertices=generateVertices,
                                               relationParamList = relationParamList,
                                               avoid_double = avoid_double)
                    offset+=1
                                
        
    def generateGraph(self,bs,paramSource,paramTarget,paramType=None,typ=None,counter=0,fh=None,vertex_type=None,
                      generateVertices=False,
                      relationParamList=[],
                      avoid_double  = True):
       
        """
        Creates edges (and vertices= from a sparql query)
        :param list bs: normallly a SPARQL binding
        :param str paramSource: variable in the query which describes the source of a edge
        :param str paramTarget variable in the query which describes the target of a edge
        :param str paramType:  (alternative to typ) variable in the query which describes the attribute typ of a edge
        :param str typ:  (alternative to paramType) string the becomes the attr type of the a edge
        :param str limit:  (optional) maximal number of query results
        :param bool generateVertices: (default = False)
        :param str vertex_type: (optional) if set sets attribute "type" of all newly created vertices to type
        :param int counter: (optional) set if counter shouldn't start with 0
        :param FileHandeler fh: (optional) if set method only creates a file with s,type,t per line.
        :param list(str) relationParamList: (optional) if set list of variables in the binding which will become attributes for the relations
        :param bool avoid_double: (optional) defaults to true, if set then double edges are avoided, currently it only checks if params are the same and source and target.
        """
        if typ is None and paramType is None:
            raise ValueError("Either typ or paramType has to be set!")
        
        if not typ is None and not paramType is None:
            raise ValueError("Either typ or paramType has to be set, not both!")
        
        addedges = []
        addvertices = set()
        
        for binds in bs:
            logger.debug("%s/%s" % (counter, len(bs)))
            try:
                s_name = binds[paramSource].value
                e_name = binds[paramTarget].value
                rdfsvars = False
            except:
                if isinstance(binds[rdflib.Variable(paramSource)], URIRef):
                    s_name = "%s"%binds[rdflib.Variable(paramSource)]
                else:
                    s_name = binds[rdflib.Variable(paramSource)].value
                if isinstance(binds[rdflib.Variable(paramTarget)], URIRef):
                    e_name = "%s"%binds[rdflib.Variable(paramTarget)]
                else:
                    e_name = binds[rdflib.Variable(paramTarget)].value
                rdfsvars = True
                
            edgeAttributes={} 
            if paramType is not None:
                if rdfsvars:
                    edgeAttributes["typ"] =  binds[rdflib.Variable(paramType)].value
                else:     
                    edgeAttributes["typ"] =  binds[paramType].value           
           
            for relParam in relationParamList:
                try:
                    if rdfsvars:
                        edgeAttributes[relParam] =  binds[rdflib.Variable(relParam)].value
                        #print(relParam,binds[rdflib.Variable(relParam)].value)
                    else:
                        edgeAttributes[relParam] =  binds[relParam].value
                    #print(relParam,binds[relParam].value)
                except KeyError: 
                    print("no result for relationParameter: %s"%relParam)
           
            if fh is not None: # filename mode, means data is written into a file not into the graph
                fh.write("%s,%s,%s\n"%(s_name,typ,e_name))
                counter+=1
            else: 
                try:
                    sv = self.vs.find(name=s_name)
                except ValueError:
                    if generateVertices:
                        # self.add_vertex(name=s_name)
                        # self.vs.find(name=s_name)["typ"]=typ #vertex bekommt den selben typ wie die relation
                        # sv = self.vs.find(name=s_name)
                        # generatedVertices.append(sv)
                        addvertices.add(s_name)
                      
                        sv = s_name
                    else:
                        sv = None
                         
                try:
                    ev = self.vs.find(name=e_name)
                except ValueError:
                    if generateVertices:
                        # self.add_vertex(name=e_name)
                        # self.vs.find(name=e_name)["typ"]=typ #vertex bekommt den selben typ wie die relation
                        # ev = self.vs.find(name=e_name)
                        # generatedVertices.append(ev)
                        addvertices.add(e_name)
                        ev = e_name
                         
                    else:
                        ev = None
                 
                 
                if ev is None or sv is None:
                    logger.debug("Nothing created, either start or end not known and generateVertices is false: %s %s"%(s_name,e_name))
                    counter += 1
                    continue  # nicht anlegen
                 
                 
                if not avoid_double or not (e_name, s_name,edgeAttributes) in addedges:    
                    addedges.append((s_name, e_name,edgeAttributes))     
                    # es = self.es.find(_source=sv.index,_target=ev.index)["typ"]=typ
                    counter += 1
        
        
        if fh is not None:
            return counter
        
        self.igraph.add_vertices(list(addvertices))  
       
        vertices = [self.vs.find(name=x) for x in addvertices]
        if vertex_type is not None:
            for v in vertices:
                v["type"]=vertex_type
        
        for s,e,t in addedges:

            self.add_edge(s, e,**t)
            
        
        return counter
             
    def addPropsToNode(self,prop_patterns):
        
        for k,p in prop_patterns.items():
            qr = self.prefixes + " select ?start_x ?end_x where {?start_x  " + p + "   ?end_x}"

            self.setQuery(qr)
            res = self.query()
            for r in res.bindings:
                n = self.vs.find(name=r["start_x"].value)
                n[k]=r["end_x"].value
        
    def add_label_to_vs(self, property_name_for_uri="name",showWidget=True):
        query = """
       
        select ?label where {
       <%s> RDFS:label ?label.
        }
        """
        labels = []
        if showWidget:
            from IPython.display import display
            from ipywidgets import FloatProgress
            f = FloatProgress(min=0, max=len(self.vs))
            display(f)
            
        f.value=0
        for v in self.vs:
            uri = v[property_name_for_uri]
            f.value+=1
            self.setQuery(query % uri)
            res = self.query()
            if len(res.bindings) > 0:
                labels.append(res.bindings[0]["label"].value)
            else:
                labels.append("--")
            
            
        self.vs["label"] = labels
    
    
    
    def add_edge_safe(self,ns_vertex,nm_vertex,node_edge_properties):
        """adds only edges which are not already existing. Checks if edge with given properties already exists
        This becomes very slow if the number of edges is high.
        """

        n_ps = set([(x,y) for x,y in node_edge_properties.items() if y])

        edges = self.es.select(_source=ns_vertex.index,_target=nm_vertex.index)
        #print("start")

        if len(n_ps) == 0 and len(edges) > 0:
            return # don't add if already an edge and no properties

        for edg in edges:

            edg_a = set([(x,y) for x,y in edg.attributes().items() if y])

            shared_items = edg_a.symmetric_difference(n_ps)

            if len(shared_items)==0:
                #print("no")
                return False #no adding necessary
            #print(shared_items,edg.attributes().items(),node_edge_properties.items())
        self.igraph.add_edge(ns_vertex,nm_vertex,**node_edge_properties)

    def chunks(self, l, n):
        """Yield successive n-sized chunks from l."""
        for i in range(0, len(l), n):
            yield l[i:i + n]

    def generateBipartiteFromSparqlParallel(self, qs, ns_e_type, nm_type, worker = 6,vattr_name="name", bindings = None,**kwargs):
        """erzeugt aus einer Sparql abfrage einen bipartiten Graphen

                :param str qs: Querystring
                :param str ns_e_type: typ des Knoten für  Start (und Ende)
                :param str nm_type: typ des anderen Knotens
                if not set I am expecting, ns, nm, ne for the nodes as Variables and ns_e_type and nm_type (ne is only necessary if graph is directed.)
        """
        edges = set()
        if bindings is None:
            self.setQuery(qs)
            res = self.query()
            bindings = res.bindings

        chunk_size =  int(len(bindings) / worker) + 1
        print("chunk_size:",chunk_size)

        bindings = self.chunks(bindings,chunk_size)

        data = []
        for binding in bindings:
            data.append([self,ns_e_type,nm_type,binding,kwargs])


        with Pool(len(data)) as p:
            graphs = p.map(generateBipartiteFromSparqlProcess,data)


        ret_gr = graphs[0]



        for gr in graphs[1:]:

            try:
                ret_gr = union(ret_gr,gr,vattr_name)
            except AssertionError:
                print(ret_gr.vs[vattr_name])
                print(gr.vs[vattr_name])



        return ret_gr


    def generateBipartiteFromSparql(self,qs,ns_e_type,nm_type,
                                    progressBar=None,
                                    tqdm=None,
                                    simplify=True,
                                    safe=True,
                                    gr = None,
                                    binding=None,
                                    identPriority=None
                                    ):
        """erzeugt aus einer Sparql-Abfrage einen bipartiten Graphen.
        Bei großen Graphen bietet sich an die Abfrage erst durchzuführen und dann als binding zu übergeben.


        :param str qs: Querystring
        :param str ns_e_type: typ des Knoten für  Start (und Ende)
        :param str nm_type: typ des anderen Knotens
        if not set I am expecting, ns, nm, ne for the nodes as Variables and ns_e_type and nm_type (ne is only necessary if graph is directed.)
        :param gr (optional): graph if not the internal graph is used
        :param binding (optional): results of a search query (query().bindings) can be used if query is very large and you want to create the query step by step.
        """

        edges=set()

        if binding is None:
            try:
                self.setQuery(qs)
                res = self.query()
                binding = res.bindings
            except:
                return gr

        if gr is None:
            gr = self

        cnt=0
        if progressBar is not None:
            try:
                progressBar.start(len(binding))
            except:
                progressBar.max=len(binding)


        if tqdm is not None:
            iter = tqdm(binding)
        else:
            iter = binding

        for r in iter:
            if progressBar is not None and hasattr(progressBar,"update"):#different progressbars
                progressBar.update(cnt)
                cnt +=1
            elif progressBar is not None and hasattr(progressBar,"value"):
                    progressBar.value+=1
                
            logger.debug(r)

            #allow priorities if nm__ident_X is in check these
            if identPriority is not None:
                for prio in range(identPriority+1):
                    if r.get("nm__ident_%s"%prio,None) is not None:
                        if prio == 0:
                            print("0")
                        nm = "%s_%s"%(nm_type,r["nm__ident_%s"%prio].value)
                        break
            else:
                nm = "%s_%s"%(nm_type,r["nm"].value)
            ns = "%s_%s"%(ns_e_type,r["ns"].value)
            try:
                ns = "%s_%s"%(ns_e_type,r["ns"].value)
            except KeyError:
                continue




            try:
                ne =  "%s_%s"%(ns_e_type,r["ne"].value)
            except KeyError:
                ne = None
                 
            #further properties
            logger.debug("VERTICES")
            logger.debug(nm)
            logger.debug(ns)
            logger.debug(ne)

            node_edge_properties=defaultdict(dict)
            for k,v in r.items():
                variablenName = "%s"%k
                try:
                    node,key = variablenName.split("__") # expect names like nm__XX
                except ValueError:
                    logger.debug("don't deal with: %s"%k)
                    continue
                node_edge_properties[node][key]=v.value
            if identPriority is not None:
                node_edge_properties[node]["ident"] = nm
            try:
                ns_vertex =gr.vs.find(name=ns)
                for k,v in node_edge_properties["ns"].items():
                            if ns_vertex[k]!=v and ns_vertex[k] is not None:
                               ns_vertex[k]=ns_vertex[k]+"||"+v
                            else:
                                ns_vertex[k] = v
            except ValueError:
                #kom_gr.add_vertex(name=k,typ="K",is_komission=True,begin=k_begin,end=k_end)
                gr.add_vertex(ns,typ=ns_e_type,**node_edge_properties["ns"])
                ns_vertex =gr.vs.find(name=ns)
            
            try:
                nm_vertex =gr.vs.find(name=nm)
                for k,v in node_edge_properties["nm"].items():
                            if nm_vertex[k]!=v and nm_vertex[k] is not None:
                                nm_vertex[k]=nm_vertex[k]+"||"+v
                            else:
                                nm_vertex[k] = v
            except ValueError:
                #kom_gr.add_vertex(name=k,typ="K",is_komission=True,begin=k_begin,end=k_end)
                gr.add_vertex(nm,typ=nm_type,**node_edge_properties["nm"])
                nm_vertex =gr.vs.find(name=nm)
            except TypeError:
                logger.error("Typerror")
            if ne is not None:
                try:
                    #if vertex exists I need to merge the properties
                    ne_vertex =gr.vs.find(name=ne)
                    for k,v in node_edge_properties["ne"].items():
                            if ne_vertex[k]!=v:
                                ne_vertex[k]=ne_vertex[k]+"||"+v
                except ValueError:
                    #kom_gr.add_vertex(name=k,typ="K",is_komission=True,begin=k_begin,end=k_end)
                    gr.add_vertex(ne,typ=ns_e_type,**node_edge_properties["ne"])
                    ne_vertex =gr.vs.find(name=ne)
         
            #self.add_edge(ns_vertex, nm_vertex,**node_edge_properties["ns_nm"])
            #print("NM-edges")
            if safe:
                gr.add_edge_safe(ns_vertex, nm_vertex,node_edge_properties["ns_nm"])
            else:
                gr.add_edge(ns_vertex, nm_vertex, **node_edge_properties["ns_nm"])
            
            try:
                if str(node_edge_properties["ns_nm"]["begin"])=="1910":
                    print (node_edge_properties)
            except KeyError:
                pass
            
            #if (ns_vertex, nm_vertex) in edges:
            #    logger.debug("Edge %s - %s exists already."%(ns_vertex, nm_vertex))
            #edges.add((ns_vertex, nm_vertex))
            if ne is not None:
                if safe:
                    gr.add_edge_safe(nm_vertex, ne_vertex,node_edge_properties["nm_ne"])
                else:
                    gr.add_edge(nm_vertex,ne_vertex,**node_edge_properties["nm_ne"])
             
        if simplify:
            def simpl(ls):
                try:
                    return ";".join(list(set(ls)))
                except TypeError:
                    return ""
                    
            gr.simplify(combine_edges=simpl)

        return gr

if __name__ == '__main__':
    
    myGraph = SPARQLGraph("http://localhost:7200/repositories/astrophysics_GMPG")
    
    query = """
     PREFIX RDF: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
     PREFIX RDFS: <http://www.w3.org/2000/01/rdf-schema#>
     PREFIX ASTRENT: <http://entities.mpiwg-berlin.mpg.de/astro/datenbank/>
     PREFIX ASTRONT: <http://ontologies.mpiwg-berlin.mpg.de/astro/>
     
     select ?uri  where {
     ?uri RDF:type ASTRONT:%s.
    
    } limit 10
    """
    
    myGraph.addNodesFromSparql(query % "actor", "actor")
    myGraph.addNodesFromSparql(query % "source", "source")
    
    query = """
     PREFIX RDF: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
     PREFIX RDFS: <http://www.w3.org/2000/01/rdf-schema#>
     PREFIX ASTRENT: <http://entities.mpiwg-berlin.mpg.de/astro/datenbank/>
     PREFIX ASTRONT: <http://ontologies.mpiwg-berlin.mpg.de/astro/>
     
     select ?s ?t  where {
     ?s ASTRONT:found_in_document ?t.
    
    } limit 10
    """
    
    myGraph.addRelationsFromSparql(query, "found_in_document", "s", "t", False)
    

        
        
    
    
    
