from distutils.core import setup

setup(
    name='SPARQLGraph',
    version='0.52',
    packages=["SPARQLGraph","helpers"],
    url='',
    license='',
    author='dwinter',
    author_email='dwinter@mpiwg-berlin.mpg.de',
    description='',
    install_requires = ["python-igraph",
                        "SPARQLWrapper",
                        "rdflib",
#                        "network_extensions@git+https://gitlab.gwdg.de/dirk.wintergruen/network_extensions.git" ##DOESN'T WORK WITH NEW PIP
    ]
)
